library(dplyr)
library(ggplot2)

dataSet <- read.csv("./us-census-2017.csv")

nameOfStates <- c("Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "District of Columbia", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming", "Puerto Rico")
countyOfState <- c()
populationList = c()

totalPopulationOfUsa = 0
for (name in nameOfStates){

    subsetData <- subset(dataSet, State == name)
    totalPopulation = select(subsetData, TotalPop)

    rowOfState <- as.numeric(unlist(totalPopulation))
    sumOfPopulation <- 0
    countyCount = 0
    for (cols in rowOfState){
        countyCount = countyCount+1
        sumOfPopulation = sumOfPopulation + cols
    }

    index <- length(populationList)
    totalPopulationOfUsa = totalPopulationOfUsa + sumOfPopulation
    populationList[index+1] <- sumOfPopulation
    countyOfState[index+1] <- countyCount
}

percentageOfStates <- c()

for(state in populationList){
    index <- length(percentageOfStates)
    percentageOfStates[index+1] = (state/totalPopulationOfUsa) * 100
}

# Formation Of Data Frames


populationDataframe <- data.frame(stateNames=nameOfStates, population=percentageOfStates)
countyStateDf <- data.frame(stateNames=nameOfStates, county=countyOfState)



# County count Distribution

maxCounty = max(countyStateDf$county)
print(paste("The state that has the highest county count is",countyStateDf$stateNames[which.max(countyStateDf$county)],", It has a Countyulation of",( maxCounty)))

minCounty = min(countyStateDf$county)
print(paste("The state that has the lowest  county count is",countyStateDf$stateNames[which.min(countyStateDf$county)],", It has a Countyulation of",( minCounty)))

meanCounty = mean(countyStateDf$county)
print(paste("Average county count Of States is ",meanCounty))



bar1 <- ggplot(data=countyStateDf, aes(x=countyOfState, y=stateNames)) +
geom_segment( aes(x=0, xend=countyOfState, y=stateNames, yend=stateNames)) + 
geom_point(size=4) 



# State Population Distribution
# Population Analysis

maxPop = max(populationDataframe$population)
print(paste("The state that has the highest population is",populationDataframe$stateNames[which.max(populationDataframe$population)],", It has a population of",( maxPop * totalPopulationOfUsa)/100))

minPop = min(populationDataframe$population)
print(paste("The state that has the lowest population is",populationDataframe$stateNames[which.min(populationDataframe$population)],", It has a population of",( minPop * totalPopulationOfUsa)/100))

meanPop = mean(populationDataframe$population)
print(paste("Average Population Of States is ", meanPop * totalPopulationOfUsa /100))

bar2 <- ggplot(data=populationDataframe, aes(x=population, y=stateNames)) +
geom_segment( aes(x=0, xend=population, y=stateNames, yend=stateNames)) + 
geom_point(size=4) 

# Race and gender

print(paste("Total Men Population in USA : ", sum(dataSet$Men)))
print(paste("Total Women Population in USA : ", sum(dataSet$Women)))

# pieChart = pie(x=c(sum(dataSet$Men),sum(dataSet$Women)),labels = c("Men","Women"), main = "Sex Distribution in USA",col=c("#00b3ff","#ff1b6b"))

raceVector = c("Hispanic", "White", "Black", "Native", "Asian", "Pacific")
volumeRace = c(0,0,0,0,0,0)

unemployed = c(1:52)
count = 0
for(name in nameOfStates){
    count = count + 1
    stateList = dataSet[dataSet$State == name,]
    volumeRace[1] <- ceiling(volumeRace[1] + sum(stateList[4]*stateList[7])/100)
    volumeRace[3] <- ceiling(volumeRace[3] + sum(stateList[4]*stateList[9])/100)
    volumeRace[2] <- ceiling(volumeRace[2] + sum(stateList[4]*stateList[8])/100)
    volumeRace[4] <- ceiling(volumeRace[4] + sum(stateList[4]*stateList[10])/100)
    volumeRace[5] <- ceiling(volumeRace[5] + sum(stateList[4]*stateList[11])/100)
    volumeRace[6] <- ceiling(volumeRace[6] + sum(stateList[4]*stateList[12])/100)

    unemployed[count] <- ceiling(sum(stateList[4]*stateList["Unemployment"]/100))
}
raceData = data.frame(race=raceVector,population=volumeRace)

bar3 <- ggplot(data=raceData, aes(x=race, y=population)) +
geom_bar(stat = "identity", width=0.2) 

print(paste("Employed People : ", totalPopulationOfUsa-sum(unemployed), ", Unemployed : ",sum(unemployed)))

employmentPieChart <- pie(x=c(sum(unemployed)/totalPopulationOfUsa*100, (totalPopulationOfUsa-sum(unemployed))/totalPopulationOfUsa*100),labels=c("Unemployed","Employed"),main="Employment Distribution",cols=c("#2fd263","#8c39db"))